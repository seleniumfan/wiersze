
Miłość
Autorem wiersza jest Jan Twardowski

Jest mi­łość trud­na
jak sól czy po pro­stu ka­mień do zje­dze­nia jest prze­wi­du­ją­ca
taka co grób za­ma­wia wciąż na dwie oso­by nie do­kład­na jak uczeń
co czy­ta po łeb­kach
jest cien­ka jak opła­tek bo we­wnątrz wzru­sze­nie
jest mi­łość wa­riat­ka ego­ist­ka ga­pa­jak je­sień lek­ko cho­ra z księ­ży­cem
kłam­czu­chem
jest mi­łość co była cia­łem a sta­ła się du­chem